using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Progress;

public class GameManager : MonoBehaviour
{
    public GameObject PrefabPlayer;
    public UISelectedMove PrefabUISelectedMove;
    public Transform tfGui;
    private bool isMoveDone;
    [SerializeField] private UISelectedMove uiSelectedMove;
    [SerializeField] private UISelectedMove uiSelectedMoveEnemy;
    private (int, int) currXYPlayer;
    private GameObject player;
    private int scorePlayer = 0;
    public Text txtScorePlayer;
    private GameObject enemy;
    private CellManager cellEnemyStart;
    private int widthBoard;
    private int heightBoard;
    private int Max = 0;
    private int posiway1;
    private int posiway2;
    private int posiway3;
    private Vector2 currXYenemy;
    private List<CellManager> listcell = new List<CellManager>();
    private List<CellManager> listcell1 = new List<CellManager>();
    private List<CellManager> listcell2 = new List<CellManager>();
    private List<CellManager> listcell3 = new List<CellManager>();
    Hashtable listcellslot = new Hashtable();
    List<string> maxPackages = new List<string>();
    //private Dictionary<int, int> listcellslot = new Dictionary<int, int>();
    private List<CellManager> listcelltemp = new List<CellManager>();
    private CellManager CellNext;
    CellManager upCell;
    CellManager downCell;
    CellManager rightCell;
    CellManager leftCell;
    CellManager way1;
    CellManager way2;
    CellManager way3;
    Vector2 up;
    Vector2 down;
    Vector2 right;
    Vector2 left;

    private void Start()
    {
        CreateUISelectedMove();
        BoardManager.INSTANCE.GenerateBoard();
        widthBoard = BoardManager.INSTANCE.Width;
        heightBoard = BoardManager.INSTANCE.Height;
        GeneratePlayerAndEnemy();
        txtScorePlayer.text = $"Score Player:{scorePlayer}";
    }

    private void OnDisable()
    {
        uiSelectedMove.actionUnRegister(Move);
    }

    private void CreateUISelectedMove()
    {
        uiSelectedMove.actionRegister(Move);
    }


    private void GeneratePlayerAndEnemy()
    {
        var (start, end) = BoardManager.INSTANCE.GetStartAndEndPointBoard();
        player = Instantiate(PrefabPlayer, BoardManager.INSTANCE.transform);
        currXYPlayer = BoardManager.INSTANCE.getXYFromID(0, 7);
        var consPlayer = new Vector2(currXYPlayer.Item1, currXYPlayer.Item2);
        var cellStart = BoardManager.INSTANCE.GetCellInBoard(consPlayer);
        cellStart.SetCellOf(1);
        player.transform.position = start;
        enemy = Instantiate(PrefabPlayer, BoardManager.INSTANCE.transform);
        var consEnemy = new Vector2(BoardManager.INSTANCE.Width - 1, BoardManager.INSTANCE.Height - 1);
        currXYenemy = consEnemy;
        cellEnemyStart = BoardManager.INSTANCE.GetCellInBoard(consEnemy);
        cellEnemyStart.SetCellOf(2);
        enemy.transform.position = end;
        ShowUI();
    }

    private void ShowUI()
    {
        uiSelectedMove.SetOnOff(true);
    }

    public void Move(float dir, int type)
    {
        var newXY = GetNewXY(dir, type);
        if (BoardManager.INSTANCE.IsOnBoard(newXY))
        {
            var cons = new Vector2();
            cons.x = newXY.Item1;
            cons.y = newXY.Item2;
            var cell = BoardManager.INSTANCE.GetCellInBoard(cons);
            if (cell.GetCellOf() == 0)
            {
                currXYPlayer = newXY;
                StartCoroutine(IEMoveToTarget(cell, player, 1));
                cell.SetCellOf(1);
            }
   
            MoveEnemy();
        }
    }

    public void MoveEnemy() //di chuyen AI
    {
        Debug.Log("Toa do: "+currXYenemy);
        CheckCellNexEnemy();
        Seer();
        if (CellNext != null)
        {
            Debug.Log(listcelltemp.Count);
            Debug.Log("Cell thuc te: " + CellNext.GetPosCell());
            cellEnemyStart = CellNext;
            StartCoroutine(IEMoveToTarget(CellNext, enemy, 2));
            CellNext.SetCellOf(2);
        }
    }

    public void checkCase2() // Check case 2
    {
        listcell1 = listcell;
        way1 = listcell1[0];
        listcell1.RemoveAt(0);
        way2 = listcell1[0];
        currXYenemy = way1.GetPosCell();
        CheckCellNexEnemy();
        posiway1 = listcell.Count;
        currXYenemy = way2.GetPosCell();
        CheckCellNexEnemy();
        posiway2 = listcell.Count;
    }

    public void checkCase3() // Check case 3
    {
        listcell2 = listcell;
        way1 = listcell2[0];
        listcell2.RemoveAt(0);
        way2 = listcell2[0];
        listcell2.RemoveAt(0);
        way3 = listcell2[0];
        currXYenemy = way1.GetPosCell();
        CheckCellNexEnemy();
        posiway1 = listcell.Count;
        currXYenemy = way2.GetPosCell();
        CheckCellNexEnemy();
        posiway2 = listcell.Count;
        currXYenemy = way3.GetPosCell();
        CheckCellNexEnemy();
        posiway3 = listcell.Count;
    }

    public void CheckDictionary()
    {


        // Tìm gói có số lượng lớn nhất
        int maxslot = 0;
        string maxDestination = "";
        foreach (DictionaryEntry cell in listcellslot)
        {
                int quantity = (int)cell.Value;
                if (quantity > maxslot)

                {
                    maxslot = quantity;
                    maxDestination = (string)cell.Key;
                }
        }

        foreach (DictionaryEntry cell in listcellslot)
        {
                int quantity = (int)cell.Value;
                string destination = (string)cell.Key;
                if (quantity == maxslot)
                {
                    maxPackages.Add((string)destination);
                }
            }

        // In kết quả
        Console.WriteLine($"Gói hàng có số lượng lớn nhất là {maxslot} và đích đến là {maxDestination}.");
        if( maxDestination == "1")
        {
            CellNext = listcelltemp[0];
        }

        if (maxDestination == "2")
        {
            listcelltemp.RemoveAt(0);
            CellNext = listcelltemp[0];
        }

        if (maxDestination == "3")
        {
            listcelltemp.RemoveAt(0);
            listcelltemp.RemoveAt(0);
            CellNext = listcelltemp[0];
        }

    }


    private void Seer() // Du Doan Tuong Lai
    {
        if (Max == 2)
        {
            Max = 0;
            int j = listcelltemp.Count;
            if (listcelltemp.Count == 2)
            {
                if (listcelltemp != null)
                {
                    way1 = listcelltemp[0];
                    listcelltemp.RemoveAt(0);
                    way2 = listcelltemp[0];
                    var rdj2 = UnityEngine.Random.Range(0, 1);
                    if (rdj2 == 0)
                    {
                        CellNext = way1;
                    }
                    else
                    {
                        CellNext = way2;
                    }
                }
            }

            if (listcelltemp.Count == 3)
            {

                if (listcelltemp != null)
                {
                    way1 = listcelltemp[0];
                    listcell2.RemoveAt(0);
                    way2 = listcelltemp[0];
                    listcell2.RemoveAt(0);
                    way3 = listcelltemp[0];
                    var rdj3 = UnityEngine.Random.Range(0, 2);
                    if (rdj3 == 0)
                    {
                        CellNext = way1;
                    }
                    if (rdj3 == 1)
                    {
                        CellNext = way2;
                    }
                    if (rdj3 == 2)
                    {
                        CellNext = way3;
                    }
                }
            }
            currXYenemy = CellNext.GetPosCell();
            return;
        }
        
    
        if( Max < 2 )
        {
            if (listcell.Count == 1)
            {
                CellNext = listcell[0];
            }
            if (listcell.Count == 2)
            {
                if (Max == 0)
                {
                    checkCase2();
                }
                if (posiway1 > posiway2)
                {
                    listcellslot.Add(posiway1, "1");
                }
                else
                {
                    if (posiway1 == posiway2)
                    {
                        if (Max == 0)
                        {
                            listcelltemp.Add(way1);
                            listcelltemp.Add(way2);
                        }
                        Max++;
                        currXYenemy = way1.GetPosCell();
                        CheckCellNexEnemy();
                        Seer();
                        currXYenemy = way2.GetPosCell();
                        CheckCellNexEnemy();
                        Seer();
                        CheckDictionary();

                    }
                    else
                    {
                        listcellslot.Add(posiway2, "2");
                    }

                }
                listcell1.Clear();
                listcellslot.Clear();
            }
            if (listcell.Count == 3)
            {
                checkCase3();
                if (posiway1 > posiway2 && posiway2 > posiway3)
                {
                    listcellslot.Add(posiway1, "1");
                    
                }
                else if (posiway1 > posiway3 && posiway3 > posiway2)
                {
                    listcellslot.Add(posiway1, "1");
                   
                }
                else if (posiway2 > posiway1 && posiway1 > posiway3)
                {
                    listcellslot.Add(posiway2, "2");
                    
                }
                else if (posiway2 > posiway3 && posiway3 > posiway1)
                {
                    listcellslot.Add(posiway2, "2");
                    
                }
                else if (posiway3 > posiway1 && posiway1 > posiway2)
                {
                    listcellslot.Add(posiway3, "3");
                    
                }
                else if (posiway3 > posiway2 && posiway2 > posiway1)
                {
                    listcellslot.Add(posiway3, "3");
                    
                }
                else if (posiway1 == posiway2 && posiway2 == posiway3)
                {
                    if (Max == 0)
                    {
                        listcelltemp.Add(way1);
                        listcelltemp.Add(way2);
                        listcelltemp.Add(way3);
                    }
                    Max++;
                    currXYenemy = way1.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way2.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way3.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    CheckDictionary();
                }
                else if (posiway1 == posiway2 && posiway2 > posiway3)
                {
                    Max++;
                    if (Max == 0)
                    {
                        listcelltemp.Add(way1);
                        listcelltemp.Add(way2);
                    }
                    currXYenemy = way1.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way2.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    CheckDictionary();
                }
                else if (posiway1 > posiway2 && posiway2 == posiway3)
                {
                    listcellslot.Add(posiway1, "1");
                }
                else if (posiway1 == posiway3 && posiway3 > posiway2)
                {
                    if (Max == 0)
                    {
                        listcelltemp.Add(way1);
                        listcelltemp.Add(way3);
                    }
                    Max++;
                    currXYenemy = way1.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way3.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    CheckDictionary();
                }
                else if (posiway1 > posiway3 && posiway3 == posiway2)
                {
                    listcellslot.Add(posiway1, "1");

                }
                else if (posiway2 == posiway3 && posiway3 > posiway1)
                {
                    if (Max == 0)
                    {
                        listcelltemp.Add(way2);
                        listcelltemp.Add(way3);
                    }
                    Max++;
                    currXYenemy = way2.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way3.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    CheckDictionary();
                }
                else if (posiway2 > posiway1 && posiway1 == posiway3)
                {
                    listcellslot.Add(posiway2, "2");
                }
                else if (posiway2 == posiway1 && posiway1 > posiway3)
                {
                    if (Max == 0)
                    {
                        listcelltemp.Add(way2);
                        listcelltemp.Add(way1);
                    }
                    Max++;
                    currXYenemy = way2.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way1.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    CheckDictionary();
                }
                else if (posiway3 > posiway1 && posiway1 == posiway2)
                {
                    listcellslot.Add(posiway3, "3");
                }
                else if (posiway3 > posiway2 && posiway2 == posiway1)
                {
                    listcellslot.Add(posiway3, "3");
                }
                else if (posiway3 == posiway1 && posiway1 > posiway2)
                {
                    if (Max == 0)
                    {
                        listcelltemp.Add(way3);
                        listcelltemp.Add(way1);
                    }
                    Max++;
                    currXYenemy = way3.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way1.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    CheckDictionary();
                }
                else if (posiway3 == posiway2 && posiway2 > posiway1)
                {
                    if (Max == 0)
                    {
                        listcelltemp.Add(way3);
                        listcelltemp.Add(way2);
                    }
                    Max++;
                    currXYenemy = way3.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    currXYenemy = way2.GetPosCell();
                    CheckCellNexEnemy();
                    Seer();
                    CheckDictionary();
    
                }
                
                
            }
            if ( listcell.Count > 3)
            {
                listcell.RemoveAt(4);
            }
            }
        if (CellNext != null)
        {
            currXYenemy = CellNext.GetPosCell();
        }
        listcell1.Clear();
        listcellslot.Clear();
        }
 


    private void CheckCellNexEnemy() //check o xung quanh
    {
        up = currXYenemy - new Vector2(0, 1);
        down = currXYenemy + new Vector2(0, 1);
        right = currXYenemy + new Vector2(1, 0);
        left = currXYenemy - new Vector2(1, 0);

        upCell = BoardManager.INSTANCE.GetCellInBoard(up);
        downCell = BoardManager.INSTANCE.GetCellInBoard(down);
        rightCell = BoardManager.INSTANCE.GetCellInBoard(right);
        leftCell = BoardManager.INSTANCE.GetCellInBoard(left);

        listcell.Clear();

        if (upCell != null && upCell.GetCellOf() == 0)
        {
                listcell.Add(upCell);
        }

        if (downCell != null && downCell.GetCellOf() == 0)
        {
                listcell.Add(downCell);
        }

        if (rightCell != null && rightCell.GetCellOf() == 0)
        {
                listcell.Add(rightCell);
        }

        if (leftCell != null && leftCell.GetCellOf() == 0)
        {
                listcell.Add(leftCell);
        }
    }

    private bool CheckCellValid(CellManager upCell)
    {
        if (upCell != null)
        {
            if ((upCell.pos.x >= 0 && upCell.pos.x <= widthBoard - 1) &&
                (upCell.pos.y >= 0 && upCell.pos.y <= heightBoard - 1) && upCell.GetCellOf() == 0)
            {
                return true;
            }
        }

        return false;
    }

    private IEnumerator IEMoveToTarget(CellManager _cell, GameObject owner, int type)
    {
        var posPlayer = owner.transform.position;
        while (Vector3.Distance(_cell.transform.position, posPlayer) > 0)
        {
            owner.transform.position = Vector3.MoveTowards(posPlayer, _cell.transform.position, 5 * Time.deltaTime);
            posPlayer = owner.transform.position;
        }

        isMoveDone = true;
        yield return new WaitUntil(() => isMoveDone);
    }

    private (int, int) GetNewXY(float dir, int type)
    {
        var (x, y) = currXYPlayer;
        switch (type)
        {
            case 0:
                y += (int)dir;
                break;
            case 1:
                x += (int)dir;
                break;
            case 2:
                y += (int)dir;
                break;
            case 3:
                x += (int)dir;
                break;
        }

        return (x, y);
    }
}